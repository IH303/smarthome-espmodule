#include "TempVented.h"

TempVented::TempVented(PubSubClient *pMqttClient) : Module(pMqttClient){
  snprintf (clientID, 6, "%s%d", MODPREID,MODID);
  snprintf (modType, 6, MODTYPE);
  time1=millis();
  time2=time1;
  pinMode(FANPIN,OUTPUT);
  nextState();
  EEPROM.begin(512);
  dht = new DHT(DHTPIN,DHTTYPE);
  dht->begin();
  delay(100);
}

void TempVented::onConnect(){
  if(!hasFirstMeasurement){
    hasFirstMeasurement=true;
    readTemp();
    readHum();
  }
  snprintf (topic, 49, "connect/%s/%s", modType,clientID);
  setPowered(EEPROM.read(400),false);
  delay(100);
  if(powered)
    snprintf (msg, 49, "on %.1f°C %.0f%%", temperature,humidity);
  else
    snprintf (msg, 49, "off - -");
  mqttClient->publish(topic,msg);
}

void TempVented::checkMsg(String topic, String payload){
  if(topic.startsWith("set")){
    if(topic.endsWith("power")){
      if(payload=="on"){
        setPowered(true,true);
      }
      else if(payload=="off"){
        setPowered(false,true);
      }
    }
  }
  else if(topic.startsWith("get")){
    if(payload=="temperature" && powered){
      readTemp();
      updateMeasurement(false,temperature,true);
    }
    else if(payload=="humidity" && powered){
      readHum();
      updateMeasurement(true,humidity,true);
    }
    else if(payload=="power"){
      updatePower(powered);
    }
  }
  else if(topic=="discover"){
    onConnect();
  }
}

void TempVented::loop(){
  Module::loop();
  if(powered){
    time2=millis();
    if(state==0){
      if(time2 - time1 >= STBY_MS){
        nextState();
      }
    }
    else if(state==1){
      if(time2 - time1 >= VENT_MS){
        nextState();
      }
    }
    else if(state==2){

      if(time2-time1 >= MESS_MS){
        time1=time2;
        float t = dht->readTemperature();
        tempAvg += t;
        Serial.print(t);
        Serial.print("; ");
        humAvg += dht->readHumidity();
        messStep++;
        if(messStep>=MESS_COUNT){
          if(!hasFirstMeasurement)
            hasFirstMeasurement=true;
          temperature = tempAvg / MESS_COUNT;
          Serial.println(temperature);
          humidity = humAvg / MESS_COUNT;
          updateMeasurement(false,temperature,true);
          updateMeasurement(true,humidity,true);
          nextState();
        }
      }
      
    }
  }
  delay(200);
}

void TempVented::nextState(){
  time1 = millis();
  state=(state+1)%3;
  if(state==0){
    digitalWrite(FANPIN,LOW);
  }
  else if(state==1){
    digitalWrite(FANPIN,HIGH);
  }
  else if(state==2){
    tempAvg=0;
    humAvg=0;
    messStep=0;
  }
}

bool TempVented::readTemp(){
  delay(100);
  float t = dht->readTemperature();
  if(t==temperature){
    return false;
  }
  else{
    temperature=t;
    return true;
  }
}

bool TempVented::readHum(){
  delay(100);
  float h = dht->readHumidity();
  if(h==humidity){
    return false;
  }
  else{
    humidity=h;
    return true;
  }
}

void TempVented::updateMeasurement(bool hum, float mess, bool valid){
  snprintf(topic,49,"update/%s/%s/%s",MODTYPE,clientID,hum?"humidity":"temperature");
  if(valid)
    snprintf(msg, 49, hum?"%.0f%%":"%.1f°C", mess);
  else
    snprintf(msg,49,"-");
  mqttClient->publish(topic,msg);
}

void TempVented::updatePower(bool power){
  String pwr = powered?"on":"off";
  snprintf(topic,49,"update/%s/%s/power",MODTYPE,clientID);
  mqttClient->publish(topic,pwr.c_str());
}

void TempVented::setPowered(bool power, bool update) {
  powered = power;
  EEPROM.write(400, powered);
  EEPROM.commit();
  if(power){
    readTemp();
    readHum();
  }
  if(update){
    updatePower(power);
    updateMeasurement(true,humidity,power);
    updateMeasurement(false,temperature,power);
  }
}
