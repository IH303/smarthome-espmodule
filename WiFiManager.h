#ifndef WiFiManager_h
#define WiFiManager_h
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include "index.h"
#include "config.h"

class WiFiManager
{
  public:
    WiFiManager();
    void loop();
    void startAP();
    String getSSID();
    String getPSK();
    String getIP();
  private:
    IPAddress *local_IP;
    IPAddress *gateway;
    IPAddress *subnet;
    ESP8266WebServer *server;
    const char* ap_ssid = "ESPModul";
    const char* ap_password = "smarthome";
    void handleRoot();
    void handleForm();
    void endlessBlink();
    String read_string(char add);
    void write_string(char add,String data);
    void saveConfig(String p_ssid, String p_psk, String p_ip);
    bool saved = false;
};

#endif
