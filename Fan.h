#ifndef fan_h
#define fan_h

#include "config.h"
#include "Arduino.h"
#include "Module.h"
#include <PubSubClient.h>
#include <EEPROM.h>
#include <PID_v1.h>

#define MODPREID "MF-"
#define MODTYPE "fan"

class Fan : public Module
{
  public:
    Fan(PubSubClient *pMqttClient);
    void checkMsg(String topic, String payload);
    void onConnect();
    void loop();
  private:
    bool autoMode, powered, usingPID;
    byte reference;
    float speed, target;
    double setpoint, input, output;
    PID *pid;
    void setPowered(bool power, bool save, bool update);
    void setMode(bool pMode, bool save, bool update);
    void setSpeed(float speed, bool save, bool update);
    void setReference(byte pClientID, bool save, bool update);
    void setTarget(float pTemp, bool save, bool update);
    void updatePower(bool power);
    void updateSpeed(float speed);
    void updateMode(bool pAutoMode);
    void updateReference(byte pClientID);
    void updateTarget(float pTemp); 
    void writeEEPROMDecimal(int address, float value);
    float readEEPROMDecimal(int address); 
};

#endif
