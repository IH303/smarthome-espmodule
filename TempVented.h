#ifndef tempvented_h
#define tempvented_h

#include "config.h"
#include "Arduino.h"
#include "Module.h"
#include <DHT.h>
#include <PubSubClient.h>
#include <EEPROM.h>

#define MODPREID "MT-"
#define MODTYPE "temp"



class TempVented : public Module
{
  public:
    TempVented(PubSubClient *pMqttClient);
    void checkMsg(String topic, String payload);
    void onConnect();
    void loop();
  private:
    DHT *dht;
    bool powered = false;
    void setPowered(bool power, bool update);
    void updatePower(bool power);
    void updateMeasurement(bool hum, float mess, bool valid);
    float temperature=0;
    float humidity=0;
    bool readTemp();
    bool readHum();

    bool hasFirstMeasurement = false;
    unsigned long time1, time2;
    byte state = 0;
    void nextState();
    byte messStep = 0;
    float tempAvg, humAvg;
};

#endif
