#ifndef led_h
#define led_h

#include "config.h"
#include "Arduino.h"
#include "Module.h"
#include <PubSubClient.h>
#include <EEPROM.h>

#define MODPREID "ML-"
#define MODTYPE "led"




class Led : public Module
{
  public:
    Led(PubSubClient *pMqttClient);
    void checkMsg(String topic, String payload);
    void onConnect();
    void loop();
  private:
    bool powered = false;
    void setPowered(bool power);
    void updatePower(bool power);
    void setColor(long color);
    void updateColor(long color);
    long hexStringToLong(String recv);
    void writeEEPROMLong(int address, long value);
    long readEEPROMLong(long address);
};

#endif
