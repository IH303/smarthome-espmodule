#include "WiFiManager.h"
#include "Arduino.h"

WiFiManager::WiFiManager(){
  WiFi.softAPdisconnect (true);
  local_IP = new IPAddress(192,168,4,1);
  gateway = new IPAddress(192,168,4,2);
  subnet = new IPAddress(255,255,255,0);
  server = new ESP8266WebServer(80);
}

void WiFiManager::loop(){
  server->handleClient();
}

void WiFiManager::startAP(){
  WiFi.mode(WIFI_AP);
  if(!WiFi.softAPConfig(*local_IP, *gateway, *subnet))
    endlessBlink();
  delay(100);
  if(!WiFi.softAP(ap_ssid, ap_password))
    endlessBlink();
  Serial.println(WiFi.localIP());
  server->on("/", std::bind(&WiFiManager::handleRoot, this));
  server->on("/action_page", std::bind(&WiFiManager::handleForm, this));
  server->begin();
}

void WiFiManager::handleRoot() {
  String s = MAIN_page;
  if(!saved) server->send(200, "text/html", s);
  else server->send(200, "text/html", "<body bgcolor=\"c4e2ef\"> <h2> Bitte Modul neustarten! </h2> </body>");
}

void WiFiManager::handleForm() {
 saveConfig(server->arg("ssid"), server->arg("psk"), server ->arg("ip"));
 server->send(200, "text/html", "<body bgcolor=\"c4e2ef\"> <h2> Einstellung wurde gespeichert! </h2> </body>");
 saved = true;
 Serial.println("Einstellung wurde gespeichert!");
}

void WiFiManager::endlessBlink(){
  while(true){
    for(byte i=0;i<3;i++){
      digitalWrite(STATUS_LED, SL_HIGH);
      delay(100);
      digitalWrite(STATUS_LED, SL_LOW);
      delay(100);
    }
    delay(1500);
  }
}

String WiFiManager::getSSID(){
  return read_string(2);
}

String WiFiManager::getPSK(){
  return read_string(35);
}

String WiFiManager::getIP(){
  return read_string(99);
}

void WiFiManager::saveConfig(String p_ssid, String p_psk, String p_ip){
  EEPROM.write(0,(byte)p_ssid.length());
  EEPROM.write(1,(byte)p_psk.length());
  write_string(2,p_ssid);
  write_string(35,p_psk);
  write_string(99,p_ip);
}

void WiFiManager::write_string(char add,String data)
{
  int _size = data.length();
  int i;
  for(i=0;i<_size;i++)
  {
    EEPROM.write(add+i,data[i]);
  }
  EEPROM.write(add+_size,'\0');
  EEPROM.commit();
}

String WiFiManager::read_string(char add)
{
  int i;
  char data[70];
  int len=0;
  unsigned char k;
  k=EEPROM.read(add);
  while(k != '\0' && len<70)
  {    
    k=EEPROM.read(add+len);
    data[len]=k;
    len++;
  }
  data[len]='\0';
  return String(data);
}
