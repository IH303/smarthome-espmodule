#include "Module.h"
#include "Arduino.h"

Module::Module(PubSubClient *pMqttClient){
  mqttClient = pMqttClient;
}

void Module::subscribe(){
  snprintf(topic,49,"set/%s/%s/+",modType,clientID);
  mqttClient->subscribe(topic);
  snprintf(topic,49,"get/%s/%s",modType,clientID);
  mqttClient->subscribe(topic);
  mqttClient->subscribe("discover");
}

void Module::loop(){
  #ifdef SLINVERSE
    analogWrite(STATUS_LED, 1023-brightness);
  #else
    analogWrite(STATUS_LED, brightness);
  #endif
  brightness = brightness + fadeAmount;
  if (brightness <= 0 || brightness >= SLMAX) {
    fadeAmount = -fadeAmount;
  }
}


boolean Module::reconnect(){
  snprintf(topic,49,"disconnect/%s",clientID);
  while (!mqttClient->connected()) {
    if (mqttClient->connect(clientID,"ESPModul","smarthome123",topic,1,false,"")) {
      Serial.println("Mit MQTT-Broker verbunden!");
      subscribe(); //(re-)abonnieren
      onConnect(); //Anmeldung mitteilen              
    } else {
      Serial.print(".");
      digitalWrite(STATUS_LED, SL_HIGH);
      delay(200);
      digitalWrite(STATUS_LED, SL_LOW);
      delay(200);
      digitalWrite(STATUS_LED, SL_HIGH);
      delay(200);
      digitalWrite(STATUS_LED, SL_LOW);
    }
  }
}
