/* ============ Konfiguration ============ */

/* ------------- Modultyp ---------------- */

  /* mögliche Modulvarianten (Wahl durch Einkommentieren einer einzelnen Zeile)*/
  //#define LED
  //#define TEMP
  //#define FAN
  //#define FANLITE
  #define TEMPVENTED
  
  /* ID des Moduls*/
  #define MODID 2

/* ------------- Allgemein --------------- */

  #define CONFIG_BUTTON D7
  #define STATUS_LED D6
  #define SLINVERSE     //SL = Status-LED
  #define SLSTEP 5
  #define SLMAX 25
  
/* ------------- LED --------------------- */

  #define red D1
  #define green D2
  #define blue D3
  #define COMMON_ANODE

/* ------------- TEMP -------------------- */

  #define DHTPIN D1
  #define DHTTYPE DHT11

/* ------------- FAN --------------------- */
  
  #define pwmPin D1
  #define stbyPin D2

/* ------------- FANLITE ----------------- */

  #define fanPin D1
  #define INVERSE

/* ------------- TEMPVENTED -------------- */

  #define DHTPIN D1
  #define DHTTYPE DHT11
  #define FANPIN D2
  #define STBY_MS 1800000 //30 min
  #define VENT_MS 300000  //5 min
  #define MESS_MS 30000   //1 min
  #define MESS_COUNT 10 

/* ======== Konfiguration - Ende ========= */


/* ======== Nicht ändern ========= */
  #ifdef SLINVERSE
    #define SL_HIGH LOW
    #define SL_LOW HIGH
  #else
    #define SL_HIGH HIGH
    #define SL_LOW LOW
  #endif
