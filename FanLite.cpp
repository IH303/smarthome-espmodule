#include "FanLite.h"

FanLite::FanLite(PubSubClient *pMqttClient) : Module(pMqttClient){
  snprintf (clientID, 6, "%s%d", MODPREID,MODID);
  snprintf (modType, 6, MODTYPE);


  pinMode(fanPin, OUTPUT);
  EEPROM.begin(512);
  autoMode=EEPROM.read(401);
  setReference(EEPROM.read(404),false,false);
  setTarget(readEEPROMDecimal(405),false,false);
  setPowered(EEPROM.read(400),false,false);  
}

void FanLite::onConnect(){
  snprintf (topic, 50, "connect/%s/%s", modType,clientID);
  snprintf (msg, 50, "%s %s %.1f %u %.1f",
    powered?"on":"off",
    autoMode?"auto":"manual",
    readEEPROMDecimal(402),
    reference,
    target);
  mqttClient->publish(topic,msg);
  setReference(reference,false,false);
}

void FanLite::checkMsg(String topic, String payload){
  if(topic.startsWith("set")){
    if(topic.endsWith("power")){
      setPowered(payload=="on",true,true);
    }
    else if(topic.endsWith("speed")){
      setSpeed(payload.toFloat(),true,true); 
    }
    else if(topic.endsWith("mode")){
      setMode(payload=="auto",true,true);
    }
    else if(topic.endsWith("ref")){
      setReference(payload.toInt(),true,true);
    }
    else if(topic.endsWith("target")){
      setTarget(payload.toFloat(),true,true);
    }
  }
  else if(topic.startsWith("update")){
    if(topic.endsWith("temperature")){
      input=payload.toDouble();
    }
  }
  else if(topic=="discover"){
    onConnect();
  }
}

void FanLite::loop(){
  Module::loop();
  if(autoMode){
    if(input > 0.5+target) setSpeed(100,false,false);
    else if(input < -0.5+target) setSpeed(0,false,false);
  }
  delay(200);
}

void FanLite::setPowered(bool power, bool save, bool update){
  powered = power;
  if(save){
    EEPROM.write(400, powered);
    EEPROM.commit();
  }
  if(powered){
    setMode(autoMode,false,false);
  }
  else{
    #ifdef INVERSE
      digitalWrite(fanPin,HIGH);
    #else
      digitalWrite(fanPin,LOW);
    #endif
    
  }
  if(update)updatePower(power);
}

void FanLite::setMode(bool pMode, bool save, bool update){
  autoMode = pMode;
  if(save){
    EEPROM.write(401, autoMode);
    EEPROM.commit();
  }
  if(!autoMode){
    setSpeed(readEEPROMDecimal(402),false,true);   
  }
  else{
    #ifdef INVERSE
      digitalWrite(fanPin,HIGH);
    #else
      digitalWrite(fanPin,LOW);
    #endif
  }
  if(update)updateMode(autoMode);
}

void FanLite::setSpeed(float pSpeed, bool save, bool update){
  speed = pSpeed<50?0:100;
  if(powered){
    if(speed==0){
      #ifdef INVERSE
      digitalWrite(fanPin,HIGH);
    #else
      digitalWrite(fanPin,LOW);
    #endif
    }
    else{
      #ifdef INVERSE
      digitalWrite(fanPin,LOW);
    #else
      digitalWrite(fanPin,HIGH);
    #endif
    }
  }
  if(save){
    writeEEPROMDecimal(402,speed);
  }
  if(update)updateSpeed(speed);
}

void FanLite::setReference(byte pClientID, bool save, bool update){
  if(reference!=pClientID){
    input=0;
    setSpeed(0,false,false);
  }
  snprintf(topic,49,"update/temp/MT-%u/temperature",reference);
  mqttClient->unsubscribe(topic);
  reference = pClientID;
  if(save){
    EEPROM.write(404, reference);
    EEPROM.commit();
  }
  mqttClient->unsubscribe("update/temp/#");
  snprintf(topic,49,"update/temp/MT-%u/temperature",pClientID);
  mqttClient->subscribe(topic);
  snprintf(topic,49,"get/temp/MT-%u",pClientID);
  mqttClient->publish(topic,"temperature");
  if(update)updateReference(reference);
}

void FanLite::setTarget(float pTemp, bool save, bool update){
  target = pTemp;
  if(save){
    writeEEPROMDecimal(405,target);
  }
  if(update)updateTarget(target);
}

void FanLite::updatePower(bool power){
  snprintf(topic,49,"update/%s/%s/power",MODTYPE,clientID);
  mqttClient->publish(topic,powered?"on":"off");
}

void FanLite::updateMode(bool pAutoMode){
  snprintf(topic,49,"update/%s/%s/mode",MODTYPE,clientID);
  mqttClient->publish(topic,pAutoMode?"auto":"manual");
}

void FanLite::updateSpeed(float pSpeed){
  snprintf(topic,49,"update/%s/%s/speed",MODTYPE,clientID);
  snprintf(msg,49,"%.1f",pSpeed);
  mqttClient->publish(topic,msg);
}

void FanLite::updateReference(byte pClientID){
  snprintf(topic,49,"update/%s/%s/ref",MODTYPE,clientID);
  snprintf(msg,49,"%u",pClientID);
  mqttClient->publish(topic,msg);
}

void FanLite::updateTarget(float pTemp){
  snprintf(topic,49,"update/%s/%s/target",MODTYPE,clientID);
  snprintf(msg,49,"%.1f",pTemp);
  mqttClient->publish(topic,msg);
}

void FanLite::writeEEPROMDecimal(int address, float value){
  byte pre = (byte)value;
  byte post = (byte)((value*10-pre*10));
  EEPROM.write(address, (byte)pre);
  EEPROM.write(address + 1, (byte)post);
  EEPROM.commit();

}

float FanLite::readEEPROMDecimal(int address){
  float pre = (float)EEPROM.read(address);
  float post = (float)EEPROM.read(address + 1);
  return pre+post/10;
}
