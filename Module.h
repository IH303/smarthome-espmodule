#ifndef Module_h
#define Module_h
#include <PubSubClient.h>
#include "config.h"

class Module
{
  public:
    Module(PubSubClient *pMqttClient);
    virtual void onConnect() = 0;
    void subscribe();
    virtual void checkMsg(String topic, String payload) = 0;
    virtual void loop();  
    boolean reconnect();
  protected:
    PubSubClient *mqttClient;
    char topic[50];
    char msg[50];
    char clientID[10];
    char modType[5];
    byte brightness = 0;
    byte fadeAmount = SLSTEP;
};

#endif
