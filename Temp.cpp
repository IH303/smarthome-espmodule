#include "Temp.h"

Temp::Temp(PubSubClient *pMqttClient) : Module(pMqttClient){
  snprintf (clientID, 6, "%s%d", MODPREID,MODID);
  snprintf (modType, 6, MODTYPE);
  EEPROM.begin(512);
  dht = new DHT(DHTPIN,DHTTYPE);
  dht->begin();
  delay(100);
}

void Temp::onConnect(){
  snprintf (topic, 49, "connect/%s/%s", modType,clientID);
  setPowered(EEPROM.read(400),false);
  delay(100);
  if(powered)
    snprintf (msg, 49, "on %.1f°C %.0f%%", temperature,humidity);
  else
    snprintf (msg, 49, "off - -");
  mqttClient->publish(topic,msg);
}

void Temp::checkMsg(String topic, String payload){
  if(topic.startsWith("set")){
    if(topic.endsWith("power")){
      if(payload=="on"){
        setPowered(true,true);
      }
      else if(payload=="off"){
        setPowered(false,true);
      }
    }
  }
  else if(topic.startsWith("get")){
    if(payload=="temperature" && powered){
      readTemp();
      updateMeasurement(false,temperature,true);
    }
    else if(payload=="humidity" && powered){
      readHum();
      updateMeasurement(true,humidity,true);
    }
    else if(payload=="power"){
      updatePower(powered);
    }
  }
  else if(topic=="discover"){
    onConnect();
  }
}

void Temp::loop(){
  Module::loop();
  if(powered){
    if(readTemp())
      updateMeasurement(false,temperature,true);
    if(readHum())
      updateMeasurement(true,humidity,true);
  }
}

bool Temp::readTemp(){
  delay(100);
  float t = dht->readTemperature();
  if(t==temperature){
    return false;
  }
  else{
    temperature=t;
    return true;
  }
}

bool Temp::readHum(){
  delay(100);
  float h = dht->readHumidity();
  if(h==humidity){
    return false;
  }
  else{
    humidity=h;
    return true;
  }
}

void Temp::updateMeasurement(bool hum, float mess, bool valid){
  snprintf(topic,49,"update/%s/%s/%s",MODTYPE,clientID,hum?"humidity":"temperature");
  if(valid)
    snprintf(msg, 49, hum?"%.0f%%":"%.1f°C", mess);
  else
    snprintf(msg,49,"-");
  mqttClient->publish(topic,msg);
}

void Temp::updatePower(bool power){
  String pwr = powered?"on":"off";
  snprintf(topic,49,"update/%s/%s/power",MODTYPE,clientID);
  mqttClient->publish(topic,pwr.c_str());
}

void Temp::setPowered(bool power, bool update) {
  powered = power;
  EEPROM.write(400, powered);
  EEPROM.commit();
  if(power){
    readTemp();
    readHum();
  }
  if(update){
    updatePower(power);
    updateMeasurement(true,humidity,power);
    updateMeasurement(false,temperature,power);
  }
}
