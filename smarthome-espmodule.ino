#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include "config.h"
#include "WiFiManager.h"

bool isAuth = false;
WiFiManager *manager = new WiFiManager();
WiFiClient espClient;
PubSubClient mqttClient(espClient);
const int mqtt_port = 4443;
char mqtt_server[253];

/* Entsprechendes Objekt wird erstellt */
#ifdef LED
  #include "Led.h"
  Module *mod = new Led(&mqttClient);
#elif defined TEMP
  #include "Temp.h"  
  Module *mod = new Temp(&mqttClient);
#elif defined FAN
  #include "Fan.h"
  Module *mod = new Fan(&mqttClient);
#elif defined FANLITE
  #include "FanLite.h"
  Module *mod = new FanLite(&mqttClient);
#elif defined TEMPVENTED
  #include "TempVented.h"
  Module *mod = new TempVented(&mqttClient);
#endif




/* Verbinden mit dem Accesspoint */
void setup_wifi() {
  WiFi.begin(manager->getSSID(), manager->getPSK());
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(STATUS_LED, SL_HIGH);
    delay(100);
    digitalWrite(STATUS_LED, SL_LOW);
    delay(100);
  }
  Serial.println("Mit WLAN verbunden!");
  randomSeed(micros());
}

/* Callback für Eintreffen einer Nachricht mit abonniertem Thema */
void callback(char* topic, byte* payload, unsigned int length){
  String tp = ""; //Umwandlung von Topic in String
  for(int i=0; i<strlen(topic); i++){
    tp.concat(topic[i]);
  } 
  String py = ""; //Umwandlung von Payload in String
  for(int i=0; i<length; i++){
    py.concat((char)payload[i]);
  }
  mod->checkMsg(tp,py); //Entsprechendes Modul verarbeitet die Nachricht
}

void setup() {
  Serial.begin(9600);
  delay(100);
  pinMode(STATUS_LED, OUTPUT);
  pinMode(CONFIG_BUTTON, INPUT);
  delay(100);
  if(digitalRead(CONFIG_BUTTON)){
    Serial.println("Konfigurations-Modus");
    isAuth=true;
    manager->startAP();
  }else{
    setup_wifi();
    snprintf (mqtt_server, 253, "%s", manager->getIP().c_str());
    Serial.print("Server-IP: ");
    Serial.println(mqtt_server);
    mqttClient.setServer(mqtt_server, mqtt_port);
    mqttClient.setCallback(callback);
  }
}

void loop() {
  if(isAuth){
    manager->loop();
  }else{
    if (!mqttClient.connected()) {
      Serial.print("verbindet..");
      mod->reconnect();
    }
    mqttClient.loop();
    mod->loop();
  }
}
