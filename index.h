const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Mit WLAN verbinden</title>
</head>
<body bgcolor="c4e2ef">
<h1> Mit WLAN und MQTT-Broker verbinden</h1>
<form action="/action_page" method="post">
SSID:<br> <input type="text" name="ssid" maxlength="32"><br><br>
Passwort:<br> <input type="password" name="psk" maxlength="63"><br><br>
Server-IP:<br> <input type="text" name="ip" maxlength="253"><br><br>
<input type="submit" value="Verbinden">
</form>
</body>
</html>
)=====";
