#include "Led.h"

Led::Led(PubSubClient *pMqttClient) : Module(pMqttClient){
  snprintf (clientID, 6, "%s%d", MODPREID,MODID);
  snprintf (modType, 6, MODTYPE);
  EEPROM.begin(512);
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);
  //setColor(readEEPROMLong(401));
  setPowered(EEPROM.read(400));
}

void Led::onConnect(){
  snprintf (topic, 50, "connect/%s/%s", modType,clientID);
  snprintf (msg, 50, "%s %s", EEPROM.read(400)?"on":"off",(String(readEEPROMLong(401),HEX)).c_str());
  mqttClient->publish(topic,msg);
}

void Led::loop(){
  Module::loop();
  delay(200);
}

void Led::checkMsg(String topic, String payload){
  if(topic.startsWith("set")){
    if(topic.endsWith("power")){
      if(payload=="on"){
        setPowered(true);
      }
      else if(payload=="off"){
        setPowered(false);
      }
    }
    else if(topic.endsWith("color")){
      long hex = hexStringToLong(payload);
      setColor(hex);  
    }
  }
  else if(topic.startsWith("get")){
    if(payload=="color"){
      updateColor(readEEPROMLong(401));
    }
    else if(payload=="power"){
      updatePower(powered);
    }
  }
  else if(topic=="discover"){
    onConnect();
  }
}

void Led::updateColor(long color){
  snprintf(topic,49,"update/%s/%s/color",MODTYPE,clientID);
  mqttClient->publish(topic,(String(color,HEX)).c_str());
}

void Led::setColor(long color) {
  if(powered){
    #ifdef COMMON_ANODE
      analogWrite(red, 1023 - (color >> 16));
      analogWrite(green, 1023 - (color >> 8 & 0xFF));
      analogWrite(blue, 1023 - (color & 0xFF));
    #else
      analogWrite(red, color >> 16);
      analogWrite(green, color >> 8 & 0xFF);
      analogWrite(blue, color & 0xFF);
    #endif
  }
  writeEEPROMLong(401,color);
  updateColor(color);
}

void Led::updatePower(bool power){
  String pwr = powered?"on":"off";
  snprintf(topic,49,"update/%s/%s/power",MODTYPE,clientID);
  mqttClient->publish(topic,pwr.c_str());
}

void Led::setPowered(bool power) {
  powered = power;
  EEPROM.write(400, powered);
  EEPROM.commit();
  if(powered){
    setColor(readEEPROMLong(401));
  }
  else{
    #ifdef COMMON_ANODE
      analogWrite(red, 1023);
      analogWrite(green, 1023);
      analogWrite(blue, 1023);
    #else
      analogWrite(red, 0);
      analogWrite(green, 0);
      analogWrite(blue, 0);
    #endif
  }
  updatePower(power);
}

long Led::hexStringToLong(String recv){
  char c[recv.length() + 1];
  recv.toCharArray(c, recv.length() + 1);
  return strtol(c, NULL, 16);
}

void Led::writeEEPROMLong(int address, long value){
    byte four = (value & 0xFF);
    byte three = ((value >> 8) & 0xFF);
    byte two = ((value >> 16) & 0xFF);
    byte one = ((value >> 24) & 0xFF);
    EEPROM.write(address, four);
    EEPROM.write(address + 1, three);
    EEPROM.write(address + 2, two);
    EEPROM.write(address + 3, one);
    EEPROM.commit();
}

long Led::readEEPROMLong(long address){
  //Read the 4 bytes from the eeprom memory.
  long four = EEPROM.read(address);
  long three = EEPROM.read(address + 1);
  long two = EEPROM.read(address + 2);
  long one = EEPROM.read(address + 3);
  return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}
