#ifndef temp_h
#define temp_h

#include "config.h"
#include "Arduino.h"
#include "Module.h"
#include <DHT.h>
#include <PubSubClient.h>
#include <EEPROM.h>

#define MODPREID "MT-"
#define MODTYPE "temp"



class Temp : public Module
{
  public:
    Temp(PubSubClient *pMqttClient);
    void checkMsg(String topic, String payload);
    void onConnect();
    void loop();
  private:
    DHT *dht;
    bool powered = false;
    void setPowered(bool power, bool update);
    void updatePower(bool power);
    void updateMeasurement(bool hum, float mess, bool valid);
    float temperature=0;
    float humidity=0;
    bool readTemp();
    bool readHum();
};

#endif
